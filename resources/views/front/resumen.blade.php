<div class="peach-gradient lighten-1 pb-5 p-2">
    <img src="/assets/img/domicilio2.png" alt="">
    <img src="/assets/img/resumen.png" alt="">
    <hr>
    @if (count(\Cart::getContent()))
        @foreach(\Cart::getContent() as $item)
            <p><span>Nombre: {{$item->name}}</span>
            <span>Precio: {{$item->price}}</span>
            <span>Cantidad: {{$item->quantity}}</span></p>
        @endforeach
        Total ${{number_format(\Cart::getSubtotal(),2)}}
        <hr>
        <a href="{{ url('catalog/pedido')}}" class="btn btn-success">Administrar Domicilio</a>
    @else
        <p><h2 style="font-size:150%">Aún no solicitas ningún platillo</h2></p>
    @endif
</div>