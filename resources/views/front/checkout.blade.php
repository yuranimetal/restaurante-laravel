<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" 
        crossorigin="anonymous"
    />
    <title></title>
    <style>
    </style>

</head>
<body>
    @extends('layouts.master')
    @section('content')
    <div class="container">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8"><img src="/assets/img/domicilio.png" alt=""></div>
        <div class="col-2"></div>
    </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-3"><img src="/assets/img/domicilio2.png" alt=""></div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-9">
                @if(Cart::getContent()->count() > 0)
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <th>CANTIDAD/UNIDAD</th>
                                    <th>PRODUCTO</th>
                                    <th>PRECIO UNITARIO</th>
                                    <th>IMPORTE</th>
                                    <th>Eliminar</th>
                                </thead>
                                <tbody>
                                    @foreach(Cart::getContent() as $item)
                                        <tr>
                                            <td>{{$item->quantity}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{number_format($item->price,2)}}</td>
                                            <td>{{number_format($item->price * $item->quantity)}}</td>
                                            <td>
                                                <form action="" method="post">
                                                    {{ csrf_field() }}  
                                                    <input type="hidden" name="id" value="{{$item->id}}">
                                                    <button  class="btn btn-danger" type="submit">x</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="2"></td>
                                        <td>SubTotal</td>
                                        <td>{{number_format(Cart::getSubTotal(),2)}}</td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-0 col-md-2 col-lg-6"></div>
                        <div class="col">
                            <div class="row">
                                <div class="col-sm-3 mr-5">
                                    <form action="" method="post">
                                        {{ csrf_field() }}  
                                        <a href="{{ url('menu')}}"  class="btn btn-warning" >volver</a>
                                    </form>
                                </div>
                                <div class="col-sm-3 mr-3">
                                    <form action="realizarPedido" method="post">
                                        {{ csrf_field() }}  
                                        <button type="submit"class="btn btn-info" >Realizar pedido</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="jumbotron">
                        <h1 style="font-size: 150%">Aún no has agregado ningún platillo a tu domicilio</h1>
                        <a href="{{ url('/menu')}}" class="btn btn-success">Ir al Menú</a>
                    </div>
                @endif
            </div>   
        </div>
    </div>
    @stop
    <script>
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
    </script>
</body>
</html>