<!DOCTYPE html>
<html lang="en">
<head>

    <style>

        i {
            color: red;
            font-size: 20px
        }

        .form-control::placeholder {
            font-size: 0.95rem;
            color: #aaa;
            font-style: italic
        }

        .form-control.shadow-0:focus {
            box-shadow: none
        }

        .display-3 {
            font-family: 'Sanchez', serif
        }

        p {
            font-family: 'Tajawal', sans-serif
        }

        .lead {
            font-family: 'Sanchez 200', serif
        }
    </style>
</head>
<body>

    <footer class="bg-white sunny-morning-gradient">
        <div class="container py-5">
            <div class="row py-3">
                <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">Ingeniearía de Sistemas</h6>
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><a href="#" class="text-muted">Facultad de Ingeniería</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Programa de ingeniería de sistemas</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Seminario de Programación</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Proyecto Final</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">México</h6>
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><img src="https://www.bakermckenzie.com/-/media/images/locations/mexico.ashx" width="200" alt="avatar image"></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">Colombia</h6>
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><img src="https://pacifista.tv/wp-content/uploads/2019/05/T%C3%ADoSam_Paz.jpg" width="200" alt="avatar image"></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">Delicias Mexicanas</h6>
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><img src="https://i.ytimg.com/vi/sSX8H2C8f1A/hqdefault.jpg" width="200" alt="avatar image"></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">Nuestras Delicias Mexicanas</h6>
                    <p class="text-muted mb-4">Clle 18 Cr 50 Ciudadela Universitaria Torobajo, Pasto, Nariño</p>
                    <ul class="list-inline mt-4">
                        <li class="list-inline-item"><a href="https://twitter.com/Andrs42001602" target="_blank" title="twitter"><i class="fab fa-2x fa-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="https://www.facebook.com/EYTDNWOBHM" target="_blank" title="facebook"><i class="fab fa-2x fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a href="https://www.instagram.com/andresius.25033.5876060/" target="_blank" title="instagram"><i class="fab fa-2x fa-instagram"></i></a></li>
                        <li class="list-inline-item"><a href="https://www.youtube.com/channel/UCy_ZBUx85-wySE4jxdnMsOg" target="_blank" title="pinterest"><i class="fab fa-2x fa-youtube"></i></a></li>
                        <li class="list-inline-item"><a href="https://www.google.com.co" target="_blank" title="vimeo"><i class="fab fa-2x fa-google"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <hr class="p-0 m-0 b-0">
        <div class="bg-light py-2 young-passion-gradient">
            <div class="container text-center">
                <p class="text-muted mb-0 py-2">© 2021 Delicias Mexicanas</p>
            </div>
        </div>
    </footer>
</body>
</html>