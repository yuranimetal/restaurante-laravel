<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="(max-width: 2000px)" href="./assets/css/style.css">
    <style>
        body {
            justify-content: center;
            text-align: center;
        }

        .container-home {
            overflow: hidden;
            width: 100%;
            display: flex;
            flex-flow: column;
        }

        .header-home {
            width: 100%;
            height: 50px;
            margin-bottom: 100px;
        }

        .navbar-color {
            background-color: #FF5733  !important;
        }

        .content-home {
            overflow: hidden;
            height: auto;
            width: 100%;
            padding: 100px;
        }
    </style>
</head>
<body>
    <header>
        <!--Navbar -->
        <nav class="mb-1 navbar navbar-expand-lg navbar-dark peach-gradient lighten-1">
            <a class="navbar-brand animated bounce infinite delay-3s slow" href="/">
                <img src="/assets/img/icono2.png" width="112" height="60" default>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-rasedul"
            aria-controls="navbarSupportedContent-rasedul" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-rasedul" style="text-shadow: 1px 1px 2px #443C3A;font-size: 150%">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                <a class="nav-link" href="{{url('/catalog')}}">Inicio</a>
                </li>
                <li class="nav-item active">
                <a class="nav-link" href="{{url('/menu')}}">Menú
                @if (Route::has('menu'))
                    <span class="sr-only">(current)</span>
                @endif
                </a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{url('/reservation')}}">Reservas</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{url('/mailbox')}}">Buzón de sugerencias</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{url('/about')}}">Nosotros</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item">
                <a class="nav-link waves-effect waves-light">
                    <i class="fas fa-cart-arrow-down"></i>
                    <a class="nav-link" href="{{ url('/catalog/pedido')}}">Domicilio @include('front.estado')</a>
                </a>
                </li>
                <li class="nav-item avatar dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-saju" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <img src="https://png.pngtree.com/png-vector/20190710/ourlarge/pngtree-user-vector-avatar-png-image_1541962.jpg" width="90" class="rounded-circle z-depth-0"
                    alt="avatar image">
                </a>
                <div class="dropdown-menu dropdown-menu-lg-right dropdown"
                    aria-labelledby="navbarDropdownMenuLink-saju">
                    <form action="{{ url('/logout') }}" method="POST" style="display:inline">
                        {{ csrf_field() }}
                        <button type="submit" class="button is-light" style="display:inline;cursor:pointer">
                            Cerrar sesión
                        </button>
                    </form>
                    <form action="{{ url('catalog/datos') }}" method="GET"  style="display:inline">
                        
                        <button type="submit" class="button is-light" style="display:inline;cursor:pointer">
                            Tu cuenta
                        </button>
                    </form>
                </div>
                </li>
            </ul>
            </div>
        </nav>
        <!--/.Navbar -->
    </header>
</body>
</html>
