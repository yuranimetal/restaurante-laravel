<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @extends('layouts.master')
    @section('content')
        <div class="row">        
        <img src="./assets/img/cuentanos.png" width="100%" height="40%">
        </div>
        <div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header peach-gradient text-white" style="text-shadow: 1px 1px 2px #705C58;">
                    Describe tus opiniones o sugerencias a continuación
                </div>
                <form action="{{ url('sugerencia') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="card-body" style="padding:30px">
                        <div class="form-group">
                            <label for="name">Nombres y Apellidos</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                        @error('name')
                            <p><strong>{{$message}}</strong></p>
                        @enderror
                        <div class="form-group">
                            <label for="phone">Teléfono</label>
                            <input type="phone" name="phone" id="phone" class="form-control">
                        </div>
                        @error('phone')
                            <p><strong>{{$message}}</strong></p>
                        @enderror
                        <div class="form-group">
                            <label for="mail">Email</label>
                            <input type="mail" name="mail" id="mail" class="form-control">
                        </div>
                        @error('mail')
                            <p><strong>{{$message}}</strong></p>
                        @enderror
                        <div class="form-group">
                            <label for="suggest">Que puedes contarnos?</label>
                            <textarea name="suggest" id="suggest" class="form-control" rows="4"></textarea>
                        </div>
                        @error('suggest')
                            <p><strong>{{$message}}</strong></p>
                        @enderror
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary aqua-gradient" style="padding:8px 100px;margin-top:25px;">
                                Enviar
                            </button>
                            <button type="submit" class="btn btn-danger ripe-malinka-gradient" style="padding:8px 100px;margin-top:25px;">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    @if(session('info'))
                        <script>
                            alert("{{session('info')}}");
                        </script>
                    @endif
                </form>
            </div>
        </div>
    </div>
    @stop
</body>
</html>