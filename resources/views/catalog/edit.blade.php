<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>edit</title>
    <style>
        body {
            font-family: 'Nunito';
        }
        .edit {
            font-zise: large !important;
            color: red;
            font-weight: bold;
        }
    </style>
</head>
<body>
    @extends('layouts.master')
    @section('content')
    <div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    Modificar película
                </div>
                <form action="{{ url('foo/bar') }}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <div class="card-body" style="padding:30px">
                        {{ method_field('PUT')}}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{$pelicula->title}}">
                        </div>

                        <div class="form-group">
                            <label for="title">Año</label>
                            <input type="date" name="date" id="date" class="form-control" value="{{$pelicula->year}}">
                        </div>

                        <div class="form-group">
                            <label for="title">Director</label>
                            <input type="text" name="text" id="director" class="form-control" value="{{$pelicula->director}}">
                        </div>

                        <div class="form-group">
                            <label for="title">Poster</label>
                            <input type="text" name="poster" id="poster" class="form-control" value="{{$pelicula->poster}}">
                        </div>

                        <div class="form-group">
                            <label for="synopsis">Resumen</label>
                            <textarea name="synopsis" id="synopsis" class="form-control" rows="3">{{$pelicula->synopsis}}</textarea>
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                                Modificar película
                            </button>
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-danger" style="padding:8px 100px;margin-top:25px;">
                                Cerrar Formulario
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @stop
</body>
</html>