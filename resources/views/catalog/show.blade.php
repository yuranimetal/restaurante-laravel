<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" 
        crossorigin="anonymous"
    />
    <title>Vista detalle</title>
    <style>
        body {
            font-family: 'Nunito';
        }
        .login {
            font-zise: large !important;
            color: red;
            font-weight: bold;
        }
    </style>
</head>
<body>
    @extends('layouts.master')
    @section('content')
        <div class="container p-5 m-5 warm-flame-gradient">
            <div class="row">
                <div class="col-sm-4">           
                    <img src="{{$platillo['imagen']}}" style="height:300px;object-fit:cover;object-position:center center;"/>       
                </div>      
                <div class="col-sm-8">   
                    <div class="row young-passion-gradient text-white p-2">
                        <div class="col"></div>
                        <div class="col"><h3>{{$platillo['titulo']}}</h3></div>
                        <div class="col"></div>
                    </div>        
                    <div class="row p-3">
                        <div class="col"><label><b>Ingredientes:</b> </label></div>
                        <div class="col text-left"><label for="">{{$platillo['ingredientes']}}</label></div>
                    </div>
                    <div class="row">
                        <div class="col"><label for=""><b>Descripción:</b></label></div>
                        <div class="col text-left"><label for="">{{$platillo['descripcion']}}</label></div>
                    </div>
                    <div class="row">
                        <div class="col"><label><b>Precio:</b></label></div>
                        <div class="col text-left"><label for="">$COP {{$platillo['precio']}} </label></div>
                    </div>
                </div> 
            </div>
            <div class="row pt-5">
                <div class="col-2"></div>
                <div class="col-8"><a href="{{ url('menu')}}"  class="btn btn-warning" >volver</a></div>
                <div class="col-2"></div>
            </div>
        </div>
    @stop
</body>
</html>