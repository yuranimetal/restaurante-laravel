<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <title>menú</title>
    <style>
        body {
            font-family: 'Nunito';
        }
        .login {
            font-zise: large !important;
            color: red;
            font-weight: bold;
        }
        h1 {
            font-size: 3.5em !important;
            margin-bottom: 20px !important;
            font-weight: bold !important;
        }
        .color-button {
            background-image: linear-gradient(to top, #30cfd0 0%, #330867 100%) !important;
        }
        #verMas:hover {box-shadow: inset 0px 0px 20px 5px red;}
        #agregarDomicilio:hover {box-shadow: inset 0px 0px 20px 5px green;}
        
    </style>
</head>
<body>
    @extends('layouts.master')
    @section('content')
    <img src="./assets/img/logo1.png" width="500" height="250">
    <div class="row">
        <img src="./assets/img/deleitate.png" width="1000" height="200">
    </div>
    <h1>Escoge el que tu prefieras!</h1>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8 p-3">
            <div class="row">
            @foreach( $arrayPlatillos as $platillo )       
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center">   
                    <div class="card card-index">
                        <div class="card-content">
                            <a href="{{ url('/catalog/show/'.$platillo->id) }}">               
                                <img src="{{$platillo['imagen']}}" style="height:250px;object-fit:cover;object-position:center center;"/>                         
                            </a> 
                            <p class="subtitle">
                                {{$platillo['titulo']}}               
                            </p>
                        </div>
                        <form action=""  method="POST">
                            {{ csrf_field() }}
                            @if($platillo['id'])
                                <input type="hidden" name="id" value="{{$platillo->id}}">
                            @endif
                            <div>
                                <div class="col"><label for="cantidad">Cantidad</label></div>
                                <div class="col"><input type="number" min="1" name="cantidad" id="cantidad" value="1" class="form-control"></div>
                            </div> 
                            <footer class="card-footer content-footer">
                                <p id="verMas" class="card-footer-item item peach-gradient">
                                <span>
                                    <a class="see-more text-white" href="{{ url('show/'.$platillo->id) }}">Ver detalles</a>
                                </span>
                                </p>
                                <p id="agregarDomicilio" class="card-footer-item item-2 morpheus-den-gradient">
                                <span>
                                    <button type="submit" class="see-more text-white color-button">Agregar al domicilio</button>
                                </span>
                                </p>
                            </footer>        
                        </form>
                    </div>
                </div>       
            @endforeach
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 p-2">
            @include('front.resumen')
        </div>
    </div>
    @stop
</body>
</html>