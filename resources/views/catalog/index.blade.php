<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Delicias Mexicanas</title>
    <style>
        body {
            font-family: 'Nunito';
        }
        .login {
            font-zise: large !important;
            color: red;
            font-weight: bold;
        }

        .see-more {
            text-decoration: none !important;
            color: #fff;
        }

        .card-index {
            margin-bottom: 20px;
        }
        h1 {
            font-size: 3.5em !important;
            margin-bottom: 20px !important;
            font-weight: bold !important;
        }

        .content-footer{
            padding: 0px !important;
        }
        .item {
            background-color: darkgreen;
        }
        .item-2 {
            background-color: #DC8612
        }
        .color-button {
            background-image: linear-gradient(to top, #30cfd0 0%, #330867 100%) !important;
        }
        #verMas:hover {box-shadow: inset 0px 0px 20px 5px red;}
        #agregarDomicilio:hover {box-shadow: inset 0px 0px 20px 5px green;}
    </style>
    
</head>
<body>
    @extends('layouts.master')
    @section('content')
        <img src="./assets/img/logo1.png" width="500" height="250">
        @include('partials.carrousel')
        <div class="row text-left">
            <div class="col-xs-12 col-sm-12 col-md-8 p-5">
                <div class="row p-3 sunny-morning-gradient">
                    <div class="col-4"><img src="/assets/img/imgIndex/1.jpg" style="height:250px;object-fit:cover;object-position:center center;"/></div>
                    <div class="col-8">
                    <h2><b>IMPORTANCIA DE LA GASTRONOMÍA</b></h2>
                        <p>A partir de esta concientización de que la gastronomía es la identidad de una región o de un país, se generó un gran interés por empezar a estudiar algunos aspectos como ingredientes o técnicas dando inicio y origen a las primeras carreras en gastronomía.</p>
                        <p>El poder transmitir de una forma estandarizada recetas y poder conocer más sobre los ingredientes, utensilios y técnicas, con el tiempo, ha sido un interés que se ha globalizado.</p>
                        <p>Lo mejor de todo es que hoy tenemos más acceso a conocimiento de otras técnicas y el tener una mejor carrera en gastronomía, es más fácil gracias a la adquisición este conocimiento.</p>
                    </div>
                </div>
                <div class="row p-3 lady-lips-gradient">
                    <div class="col-4">
                        <img src="/assets/img/imgIndex/2.jpg" style="height:250px;object-fit:cover;object-position:center center;"/>
                    </div>
                    <div class="col-8">
                        <h2><b>Gastronomía de México</b></h2>
                        <p>La gastronomía de México tiene una gran diversidad de platos típicos, por ello fue reconocida, por la UNESCO, como Patrimonio Inmaterial de la Humanidad. Los ingredientes básicos y representativos de los platillos mexicanos son: el maíz, el cilantro, el chile, el frijol, el piloncillo, el nopal y el jitomate.</p>
                        <p>La cocina mexicana, también se caracteriza por sus salsas, que sirven de acompañamiento para los platos tradicionales, preparadas a base de especies.</p>
                    </div>
                </div>
                <div class="row p-3 young-passion-gradient">
                    <div class="col-4"><img src="/assets/img/imgIndex/3.jpg" style="height:250px;object-fit:cover;object-position:center center;"/></div>
                    <div class="col-8">
                        <p><h2><b>RECETAS DE FAMILIA</b></h2></p>
                        <p>Pozoles, frijoles, huazontles, tlacoyos, sopes, nopales, aguas frescas… todo lo que comemos durante las fiestas patrias formaba parte de la mesa cotidiana de nuestros antepasados, y aunque solemos considerarlos garnachas, si se elaboran con poca grasa y priorizando el consumo de frutas y verduras, podrían ser gran parte de la solución a este problema, como menciona Gálvez Mariscal: “Los ingredientes típicos de nuestra gastronomía están cargados de fitoestrógenos, isoflavonoides, antocianinas y sulforafanos, entre otras sustancias que protegen nuestra salud por ser antioxidantes, evitan accidentes cardiovasculares, mejoran la visión y contienen una buena cantidad de vitaminas”.</p>
                    </div>
                </div>
                <div class="row p-3 dusty-grass-gradient">
                    <div class="col-4">
                        <img src="/assets/img/imgIndex/5.jpg" style="height:250px;object-fit:cover;object-position:center center;"/>
                    </div>
                    <div class="col-8">
                        <p><h2><b>CUESTIÓN DE TIEMPO</b></h2></p>
                        <p>De acuerdo con el documento de la OCDE, las personas con un nivel educativo básico, sobre todo las mujeres, tienen más posibilidades de padecer sobrepeso. En parte por la introducción de otras gastronomías al mercado, además del estilo de vida sedentario y acelerado que no permite destinar tiempo cotidiano a la planeación y preparación de la comida.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4">
                <div class="row">
                    <iframe src="https://www.visitmexico.com/" width="100%" height="450" frameborder="0"></iframe>
                </div>
                <div class="row">
                    <iframe src="https://turismo.org/gastronomia-de-mexico/" width="100%" height="450" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/JrlalXQAot0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15955.753099192838!2d-77.27044783022461!3d1.2034321999999928!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e2ed4eb1dfc4711%3A0xada8646836e2a4fa!2sD&#39;Gusto!5e0!3m2!1ses-419!2sco!4v1613103695129!5m2!1ses-419!2sco" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
        @if(session('info'))
            <script>
                alert("{{session('info')}}");
            </script>
        @endif
        @if(session('reservaInfo'))
            <script>
                alert("{{session('reservaInfo')}}");
            </script>
        @endif
    @stop
</body>
</html>