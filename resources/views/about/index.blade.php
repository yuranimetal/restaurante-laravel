<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="./assets/css/styles.css" rel="stylesheet" />

</head>
<body>
    @extends('layouts.master')
    @section('content')
    <section class="page-section" id="about">
        <div class="container">
            <div class="text-center">
            <img src="./assets/img/conocenos.png" width="800" height="80">
                <h3 class="section-subheading text-muted">La autentica cocina mexicana.</h3>
            </div>
            <ul class="timeline">
                <li>
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/1.jpg" alt="" /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Misión</h4>
                            <h4 class="subheading">De México para el mundo</h4>
                        </div>
                        <div class="timeline-body"><p class="text-muted">Preservar y difundir la tradición y gastronomía mexicana con sus colores, olores y sabores a nivel nacional e internacional invitando a nuestros comensales a repetir la experiencia, ofreciendo productos y servicios de alta calidad, a precio justo; logrando un ambiente alegre, familiar y cordial.!</p></div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/2.jpg" alt="" /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Visión</h4>
                            <h4 class="subheading">De México para el mundo</h4>
                        </div>
                        <div class="timeline-body"><p class="text-muted">Lograr el crecimiento y desarrollo del Restaurante trascendiendo con ello a otras generaciones.!</p></div>
                    </div>
                </li>
                <li>
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/3.jpg" alt="" /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Diciembre 2019</h4>
                            <h4 class="subheading">Todos somos México</h4>
                        </div>
                        <div class="timeline-body"><p class="text-muted">Desde su inicio en 2019 en Delicias Mexicanas nos hemos dedicado a preservar las recetas mexicanas y a hacer vivir a nuestros comensales la experiencia de sabor, que sólo Delicias Méxicanas ha sabido conservar.</p></div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="https://www.un.org/ruleoflaw/wp-content/uploads/2015/04/unesco.png" alt="" /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Noviembre 2010</h4>
                            <h4 class="subheading">La UNESCO reconoció a la comida mexicana como patrimonio intangible de la humanidad.</h4>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>    
    <!-- Team-->  
    <section class="page-section bg-light" id="team">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Nuestro Equipo</h2>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" 
                            src="https://scontent-bog1-1.xx.fbcdn.net/v/t1.0-9/143508748_10220310287442382_4497182056904706751_o.jpg?_nc_cat=102&ccb=3&_nc_sid=09cbfe&_nc_ohc=_QXfoCp4JogAX_FTvUp&_nc_ht=scontent-bog1-1.xx&oh=1d0026f410196befaba4f5b9d7b35e8b&oe=604BA0AD" 
                            alt="" 
                            style="object-fit:cover;object-position:center top;"/>
                        <h4>Estefany Diaz</h4>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" 
                            src="https://scontent-bog1-1.xx.fbcdn.net/v/t1.0-9/83522675_123456465845894_5021779077921505280_n.jpg?_nc_cat=108&ccb=3&_nc_sid=174925&_nc_ohc=bfZa5ZjKz3MAX8bRGSW&_nc_oc=AQmdNbxf6QlzQQyooF1i4oYSHf7e67PZgTd4QkK6RT0ad8lZZj4-yrq0SC5V9eXwXKc&_nc_ht=scontent-bog1-1.xx&oh=5d50f3598cfdad6183facc34776c4ca1&oe=60498F0A" 
                            alt=""
                            style="object-fit:cover;object-position:center center;" />
                        <h4>Carlos Ocaña</h4>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 mx-auto text-center"><p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p></div>
            </div>
        </div>
    </section>               
    @stop
</body>
</html>