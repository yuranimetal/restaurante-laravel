<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @extends('layouts.master')
    @section('content')
    <div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
        <img src="./assets/img/logo1.png" width="500" height="250">
            <div class="card">
                <div class="card-header peach-gradient text-white" style="text-shadow: 1px 1px 2px #705C58;">
                    Solicitar Reserva
                </div>
                <form action="" method="POST">
                    {{ csrf_field() }}
                    <div class="card-body" style="padding:30px">
                    <div class="form-group">
                            <label for="name">Nombres y Apellidos</label>
                            <input type="text" name="nombre" id="nombre" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="phone">Teléfono</label>
                            <input type="phone" name="telefono" id="telefono" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="date">Fecha para la reserva</label>
                            <input type="date" name="fecha" id="fecha" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="time">Hora</label>
                            <input type="time" name="hora" id="hora" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="comensales">Numero de comensales</label>
                            <select name="comensales" id="comensales" class="form-control">
                                <option value="1">Una persona</option> 
                                <option value="2">Dos personas</option> 
                                <option value="3">Tres personas</option>
                                <option value="4">Cuatro personas</option> 
                                <option value="5">Cinco personas</option> 
                                <option value="6">Seis personas</option> 
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="observaciones">Observaciones</label>
                            <textarea name="observaciones" id="observaciones" class="form-control" rows="3"></textarea>
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary aqua-gradient" style="padding:8px 100px;margin-top:25px;">
                                Solicitar
                            </button>
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-danger ripe-malinka-gradient" style="padding:8px 100px;margin-top:25px;">
                                Cancelar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @stop
</body>
</html>
