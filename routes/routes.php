<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\CarController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', [HomeController::class, 'getHome']);

Route::group(['middleware' => 'auth'], function() {
    Route::get('catalog', [CatalogController::class,'getIndex'])->name('inicio');
    Route::get('show/{id}', [CatalogController::class, 'getShow'] );
    Route::post('reservation', [CatalogController::class, 'postCreateReservation'])->name('reserva');
    Route::get('reservation', [CatalogController::class, 'getCreateReservation']);
    Route::get('menu', [CatalogController::class,'getMenu']);
    Route::get('mailbox', [CatalogController::class,'getMailbox'])->name('buzon');
    Route::post('sugerencia', [CatalogController::class,'contacto']);
    Route::get('about', [CatalogController::class,'getAbout']);
    Route::post('catalog', [CatalogController::class, 'postAgregar']);
    Route::post('menu', [CatalogController::class, 'postAgregar']);
    Route::get('catalog/pedido/', [CatalogController::class, 'checkout']);
    Route::post('catalog/pedido', [CatalogController::class, 'remover']);
    Route::post('catalog/realizarPedido', [CatalogController::class, 'realizarPedido']);
    Route::get('catalog/datos', [CatalogController::class, 'getDetalles']);
    //Route::get('car', [CarController::class,'getIndex']);
    //Route::get('catalog/create', [CatalogController::class, 'getCreate'] );
    //Route::get('catalog/edit/{id}', [CatalogController::class, 'getEdit']);
    //Route::post('catalog/create', [CatalogController::class, 'postCreate']);
    //Route::put('catalog/edit/{id}', [CatalogController::class, 'putEdit']);
    //Route::get('catalog/pedido/{id}', [CatalogController::class, 'getPedido']);
    //Route::post('catalog/pedido/{id}', [CatalogController::class, 'postCreatePedido']);
    // Route::post('catalog/pedido', [CatalogController::class, 'vaciar']);
    // rutas
} );