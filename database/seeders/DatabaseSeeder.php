<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Platillo;
use App\Models\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     *
     */
    private $arrayPlatillos = array(
        array(
            'titulo' => 'Tacos al pastor',
            'imagen' => 'http://www.comedera.com/wp-content/uploads/2017/08/tacos-al-pastor-receta.jpg',
            'descripcion' => 'Los tacos al pastor, de adobada o de trompo ​ son la variedad de tacos más populares en la mayor parte de México, ​​ originados en la Ciudad de Puebla', 
            'ingredientes' => 'Tortillas de maiz, carne de cerdo adobada, piña en rebanadas, cebolla (opcional), cilantro (opcional), salsa picante de la casa.',
            'precio' => 15000
        ),
        array(
            'titulo' => 'Tacos dorados',
            'imagen' => 'https://somoscocineros.com/assets/uploads/recipes/107/receta-mexicana-tacos-dorados-de-pollo-920-520.png',
            'descripcion' => 'El taco dorado es un platillo de México y América Central muy popular en otros países alrededor del mundo, que consiste en una pequeña tortilla, enrollada con algún tipo de relleno, normalmente a base de carne de ternera o pollo',
            'ingredientes' => 'Tortillas de maiz, carne de res o pollo, queso, crema espesa, lechuga, zanahoria y salsa picante de la casa.',
            'precio' => 13000
        ),
        array(
            'titulo' => 'Tacos de Birria',
            'imagen' => 'https://vadetacos.com/wp-content/uploads/2020/06/receta-tacos-de-birria.png', 
            'descripcion' => 'Es un plato a base de carne de carnero estrictamente (aunque en algunas regiones se prepara con carne de chivo y en algunas partes del norte, con carne de res)',
            'ingredientes' => 'Tortillas de maiz, carne de carnero o res,  cebolla (opcional), cilantro (opcional), salsa de la casa y consome',
            'precio' => 8000
        ),
        array(
            'titulo' => 'Tacos de Canasta',
            'imagen' => 'https://elgourmet.s3.amazonaws.com/recetas/cover/mole-_9IMJjkql3RCWT1eg6AHUD8PKyZLh7o.png', 
            'descripcion' => 'Los tacos de canasta son una popular preparación culinaria mexicana que consiste en tortillas rellenas de diversos guisados',
            'ingredientes' => 'Tortillas de maiz rellenas de  papa, chorizo, chicharon prensado, frijol y queso todo bañado con nuestra salsa de la casa ',
            'precio' => 17000
        ),
        array(
            'titulo' => 'Mole',
            'imagen' => 'https://mas-mexico.com.mx/wp-content/uploads/2018/03/tacos-de-canasta.jpg', 
            'descripcion' => 'El término mole se refiere a varios tipos de salsas mexicanas muy condimentadas hechas principalmente a base de chiles y especias, y que son espesadas con masa de maíz, tortilla o pan',
            'ingredientes' => 'Tortillas de maiz, mole con pollo, arroz y salsa de la casa ',
            'precio' => 25000
        )
    );

    private $arrayUsers = array(
        array(
            'name' => 'Estefany Yurani',
            'email' => 'eytd@udenar.edu.co', 
            'password' => '1234'
        ),
        array(
            'name' => 'Mugui',
            'email' => 'yuranimetal@gmail.com', 
            'password' => '5678'
        )
    );

    private function seedPlatillos() {
        \DB::table('platillos')->delete();
        foreach( $this->arrayPlatillos as $platillo ) {
            $p = new Platillo;
            $p->titulo = $platillo['titulo'];
            $p->imagen = $platillo['imagen'];
            $p->descripcion = $platillo['descripcion']; 
            $p->ingredientes = $platillo['ingredientes']; 
            $p->precio = $platillo['precio']; 
            $p->save();
        }
    }



    private function seedUsers () {
        \DB::table('users')->delete();
        foreach( $this->arrayUsers as $user ) {
            $u = new User;
            $u->name = $user['name'];
            $u->email = $user['email'];
            $u->password = bcrypt( $user['password'] );
            $u->save();
        } 
    }
    
    public function run()
    {
        self::seedPlatillos();
        self::seedUsers();
        $this->command->info('Tabla catálogo inicializada con datos!');
    }
}
