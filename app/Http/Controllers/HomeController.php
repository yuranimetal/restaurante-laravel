<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getHome()
    {
        return redirect()->action([CatalogController::class, 'getIndex']);    
        if ( Auth::User() ) {
            return view('auth.login');
        }else{
            return view('home');
        }
        //
    }

    public function __construct() {
        $this->middleware('auth');
    }
}

