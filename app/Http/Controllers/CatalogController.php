<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\platillo;
use App\Models\Reserva;
use App\Models\Pedido;
use App\Models\Movie;
use App\Models\User;
use Cart;
use Auth;
use App\Mail\SugerenciasMaillable;
use Illuminate\Support\Facades\Mail;


class CatalogController extends Controller
{
    public function getIndex() {
        $platillo = Platillo::all();
        return view('catalog.index', array('arrayPlatillos'=> $platillo ));
    }

    public function getShow($id) {
        $platillo = Platillo::findOrFail($id);
        return view('catalog.show', array('platillo'=> $platillo ));
    }

    public function getCreate() {
        return view('catalog.create');
    }

    public function getEdit($id) {
        $platillo = Platillo::findOrFail($id);
        return view('catalog.edit', array('platillo'=> $platillo ));
    }

    public function getCreateReservation() {
        return view('reservation.index');
    }

    public function getMenu() {
        $platillo = Platillo::all();
        return view('catalog.menu', array('arrayPlatillos'=> $platillo ));
    }

    public function getDetalles() {
        return view('catalog.datosCuenta');
    }

    public function getMailbox() {
        return view('mailbox.index');
    }
    public function contacto(Request $request) {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'mail' => 'required',
            'suggest' => 'required'
        ]);
        $correo = new SugerenciasMaillable($request->all());
        Mail::to('charl25033@gmail.com')->send($correo);
        return redirect()->route('buzon')->with('info','Su sugerencia ha sido enviada con éxito');
    }

    public function realizarPedido() {
        return redirect()->route('inicio')->with('info','Su pedido se ha añadido con éxito');
    }

    public function getAbout() {
        return view('about.index');
    }

    public function getPedido($id) {
        $platillo = Platillo::findOrFail($id);
        return view('catalog.pedido', array('platillo'=> $platillo ));
    }
    public function postCreate(Request $request)
    {
        $newMovie = new Movie();
        $newMovie->title = $request->title;
        $newMovie->year = $request->year;
        $newMovie->director = $request->director;
        $newMovie->poster = $request->poster;
        $newMovie->synopsis = $request->synopsis;
        $newMovie->save();
        return redirect('/catalog');
    }

    public function postCreateReservation(Request $request)
    {
        $newReserva = new Reserva();
        $newReserva->nombre = $request->nombre;
        $newReserva->telefono = $request->telefono;
        $newReserva->fecha = $request->fecha;
        $newReserva->hora = $request->hora;
        $newReserva->comensales = $request->comensales;
        $newReserva->observaciones = $request->observaciones;
        $newReserva->save();
        return redirect()->route('inicio')->with('reservaInfo','Su reserva ha sido solicitada con éxito');
    }

    public function putEdit(Request $request, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->title = $request->title;
        $movie->year = $request->year;
        $movie->director = $request->director;
        $movie->poster = $request->poster;
        $movie->synopsis = $request->synopsis;
        $movie->save();
        return redirect('/catalog/show/'.$id);
    }

    public function postCreatePedido(Request $request, $id)
    {
        $newPedido = new Pedido();
        $newPedido->platillo = $id;
        $newPedido->cantidad = $request->cantidad;
        $newPedido->save();
        return redirect('/menu');
    }

    public function postAgregar(Request $request) {
        $platillo = Platillo::find($request->id);
        \Cart::add(
            $platillo->id,
            $platillo->titulo,
            $platillo->precio,
            $request->cantidad
        );
        return back()->with('success',"Platillo se ha agregado a su adomicilio");
    }

    public function checkout() {
        return view('front.checkout');
    }

    public function remover(Request $request) {
        $platillo = Platillo::find($request->id);
        \Cart::remove(['id'=> $request->id]);
        return back()->with('success',"Platillo se ha eliminado");
    }

    /*public function vaciar() {
        Cart::clear();
        return back()->with('success',"Adomicilio vacio");
    }*/
}
